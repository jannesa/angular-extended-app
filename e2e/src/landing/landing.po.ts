import { navigateTo } from '@common-library/testing';
import { by, element } from 'protractor';

export class LandingPage {
	async navigateTo(): Promise<unknown> {
		return navigateTo('/app');
	}

	async getTitleText(): Promise<string> {
		return element(by.css('app-root h1')).getText();
	}

	async getContentText(): Promise<string> {
		return element(by.css('app-root #content p')).getText();
	}
}
