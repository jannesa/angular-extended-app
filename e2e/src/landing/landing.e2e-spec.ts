import { getConsoleLogs } from '@common-library/testing';
import { logging } from 'protractor';
import { LandingPage } from './landing.po';

describe('Landing page', () => {
	const page = new LandingPage();

	it('should open custom landing page', async () => {
		await page.navigateTo();
		expect(await page.getTitleText()).toEqual('Custom title');
		expect(await page.getContentText()).toEqual('Custom landing page!');
	});

	afterEach(async () => {
		// Assert that there are no errors emitted from the browser
		const logs = await getConsoleLogs();
		expect(logs).not.toContain(jasmine.objectContaining({
			level: logging.Level.SEVERE,
		} as logging.Entry));
	});
});
