import { Component } from '@angular/core';
import { WidgetComponent } from '@common-library/widgets';

@Component({
	selector: 'app-custom-widget',
	template: `
		<p>{{ data.text }}</p>
		<p>Custom widget translation: {{ 'app.widgets.custom.help_text' | translate }}</p>
		<p>Common library translation: {{ 'common.yes' | translate }}</p>
	`
})
export class CustomWidgetComponent implements WidgetComponent {
	data: any;
}
