import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { CommonLibraryModule, HeaderConfig, WidgetService, WidgetItem } from '@common-library';
import { LandingPageModule } from './landing-page/landing-page.module';
import { CustomWidgetComponent } from './widgets/custom-widget.component';
import { HttpClientModule } from '@angular/common/http';
import { TranslateModule, TranslateService, TranslateStore } from '@ngx-translate/core';

@NgModule({
	imports: [
		BrowserModule,
		HttpClientModule,
		TranslateModule,
		RouterModule,
		AppRoutingModule,
		CommonLibraryModule,
		LandingPageModule,
	],
	declarations: [
		AppComponent,
		CustomWidgetComponent,
	],
	providers: [
		TranslateStore,
	],
	bootstrap: [AppComponent]
})
export class AppModule {

	constructor(header: HeaderConfig, widgets: WidgetService, translate: TranslateService) {
		// Required! Don't remove!
		translate.use('en');

		header.title = 'Custom title';
		widgets.addWidget(new WidgetItem(CustomWidgetComponent, { text: 'Hi from custom widget!' }));
	}

}
