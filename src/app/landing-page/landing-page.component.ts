import { Component } from '@angular/core';

@Component({
	selector: 'app-landing-page',
	template: `<p>Custom landing page!</p>`
})
export class LandingPageComponent { }
